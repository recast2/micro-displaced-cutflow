
If you have root installed:
```
source runGetCutFlow.sh location/to/output_mc16a_from_event_selection_alg.root location/to/output_mc16d_from_event_selection_alg.root location/to/output_mc16e_from_event_selection_alg.root &> output.txt&
```
e.g.
```
source runGetCutFlow.sh mc16a.root mc16d.root mc16e.root&> output.txt &
```

With Docker:
```
docker run --rm -it -v $PWD:/Test rootproject/root:6.28.04-fedora37 bash
cd Test
source runGetCutFlow.sh mc16a.root mc16d.root mc16e.root&> output.txt &
```