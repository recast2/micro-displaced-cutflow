vector<double> getnums (TString file_name){
  TFile * file_mc16a = new TFile(file_name);

  TTree *tree = (TTree*)file_mc16a->Get("trees_MEDIUMD0_");
  ROOT::RDataFrame df("trees_MEDIUMD0_", file_mc16a);

  TH1D *MetaData = (TH1D*)file_mc16a->Get("MetaData_EventCount");
  Double_t initalSumOfWeights = MetaData->GetBinContent(3);
  Double_t initalNumEvents = MetaData->GetBinContent(1);
  Double_t selectedNumEvents = MetaData->GetBinContent(2);
  Double_t selectedSumOfWeights = MetaData->GetBinContent(4);
  /*
  float xSection = 0;
  float lumi = 0;
  float filterEff = 0;
  float kfactor = 0;
  tree->SetBranchAddress("xSection",&xSection);
  tree->SetBranchAddress("lumi",&lumi);
  tree->SetBranchAddress("filterEff",&filterEff);
  tree->SetBranchAddress("kFactor",&kfactor);
  tree->GetEntry(0);

  float effective_num_events = lumi * xSection * filterEff * kFactor;
  */

  auto define_norm_weight = [=](float &evt_w){
    return (evt_w)/initalSumOfWeights;
  };

  auto df_normEvtWeight = df.Define("normEventWeight",define_norm_weight,{"eventWeight"});

  auto df_event_weights = df_normEvtWeight.Define("normEvtWeightFinal","double weight= normEventWeight * totalMuonSF; return weight;"); //includes trigger SFs as well as muon1sf and muon2sfs

  //-----------------------------------------------------------------//
  //SUSY2 requirements and n muon >=2 and event cleaning, without scale factors as scale factors are only defined for signal muons i.e. after cuts of e.g. 20 pt
  auto susy2_2mu = df_event_weights.Count();
  float weighted_susy2_2mu = 0;
  df_event_weights.Foreach([&](double weight){weighted_susy2_2mu +=weight;}, {"normEventWeight"});

  //-----------------------------------------------------------------//
  //now also with the trigger
  auto df_trig = df_event_weights.Filter("passHLT_2mu14");
  auto trigger = df_trig.Count();
  float weighted_trigger=0;
  df_trig.Foreach([&](double weight){weighted_trigger +=weight;}, {"normEventWeight"});

  auto df_trig_from_susy2 = df_event_weights.Filter("passHLT_2mu14");
  auto trig_from_susy = df_trig_from_susy2.Count();
  float weighted_trig_from_susy = 0;
  df_trig_from_susy2.Foreach([&](double weight){weighted_trig_from_susy +=weight;}, {"normEventWeight"});

  //-----------------------------------------------------------------//
  //now also with both muons trigger matched
  auto df_trig_matched = df_trig.Filter("bothMusPassTriggerMatching");
  auto trig_matched = df_trig_matched.Count();
  float weighted_trig_matched = 0;
  df_trig_matched.Foreach([&](double weight){weighted_trig_matched +=weight;}, {"normEventWeight"});

  auto df_trig_matched_from_susy2 = df_trig.Filter("passHLT_2mu14");
  auto trig_matched_from_susy = df_trig_matched_from_susy2.Count();
  float weighted_trig_matched_from_susy = 0;
  df_trig_matched_from_susy2.Foreach([&](double weight){weighted_trig_matched_from_susy +=weight;}, {"normEventWeight"});

  //-----------------------------------------------------------------//
  //now also with opposite sign events
  auto df_opp_sign = df_trig_matched.Filter("isOppSignMuonEvent");
  auto opp_sign = df_opp_sign.Count();
  float weighted_opp_sign = 0;
  df_opp_sign.Foreach([&](double weight){weighted_opp_sign +=weight;}, {"normEventWeight"});

  auto df_opp_sign_from_susy2 = df_event_weights.Filter("isOppSignMuonEvent");
  auto opp_sign_from_susy = df_opp_sign_from_susy2.Count();
  float weighted_opp_sign_from_susy = 0;
  df_opp_sign_from_susy2.Foreach([&](double weight){weighted_opp_sign_from_susy +=weight;}, {"normEventWeight"});
  
  //-----------------------------------------------------------------//
  //now also with medium quality for both muons
  auto df_medium_qual = df_opp_sign.Filter("BothMuonsMedium");
  auto medium_qual = df_medium_qual.Count();
  float weighted_medium_qual = 0;
  df_medium_qual.Foreach([&](double weight){weighted_medium_qual +=weight;}, {"normEventWeight"});

  auto df_medium_qual_from_susy2 = df_event_weights.Filter("BothMuonsMedium");
  auto medium_qual_from_susy = df_medium_qual_from_susy2.Count();
  float weighted_medium_qual_from_susy = 0;
  df_medium_qual_from_susy2.Foreach([&](double weight){weighted_medium_qual_from_susy +=weight;}, {"normEventWeight"});
  
  //-----------------------------------------------------------------//
  //now also with |eta| < 2.5 for both muons
  auto df_eta2p5 = df_medium_qual.Filter("BothMuonsEta2p5");
  auto eta2p5 = df_eta2p5.Count();
  float weighted_eta2p5 = 0;
  df_eta2p5.Foreach([&](double weight){weighted_eta2p5 +=weight;}, {"normEventWeight"});

  auto df_eta2p5_from_susy2 = df_event_weights.Filter("BothMuonsEta2p5");
  auto eta2p5_from_susy = df_eta2p5_from_susy2.Count();
  float weighted_eta2p5_from_susy = 0;
  df_eta2p5_from_susy2.Foreach([&](double weight){weighted_eta2p5_from_susy +=weight;}, {"normEventWeight"});
  
  //-----------------------------------------------------------------//
  //now also with pt > 20 for both muons
  auto df_pt20 = df_eta2p5.Filter("BothMuonsPt20");
  auto pt20 = df_pt20.Count();
  float weighted_pt20 = 0;
  df_pt20.Foreach([&](double weight){weighted_pt20 +=weight;}, {"normEventWeight"});

  auto df_pt20_from_susy2 = df_event_weights.Filter("BothMuonsPt20");
  auto pt20_from_susy = df_pt20_from_susy2.Count();
  float weighted_pt20_from_susy = 0;
  df_pt20_from_susy2.Foreach([&](double weight){weighted_pt20_from_susy +=weight;}, {"normEventWeight"});
  
  //-----------------------------------------------------------------//
  //now also with both muons isolated
  auto df_iso = df_pt20.Filter("bothMuPassIso");
  auto iso = df_iso.Count();
  float weighted_iso = 0;
  df_iso.Foreach([&](double weight){weighted_iso +=weight;}, {"normEventWeight"});

  auto df_iso_from_susy2 = df_event_weights.Filter("bothMuPassIso");
  auto iso_from_susy = df_iso_from_susy2.Count();
  float weighted_iso_from_susy = 0;
  df_iso_from_susy2.Foreach([&](double weight){weighted_iso_from_susy +=weight;}, {"normEventWeight"});

  //-----------------------------------------------------------------//
  //now with SFs
  float weighted_iso_SFs = 0;
  df_iso.Foreach([&](double weight){weighted_iso_SFs +=weight;}, {"normEvtWeightFinal"});
    
  //-----------------------------------------------------------------//
  //now also with mll 110 gev
  auto df_mll = df_iso.Filter("invMassTwoMuons > 110");
  auto mll = df_mll.Count();
  float weighted_mll = 0;
  df_mll.Foreach([&](double weight){weighted_mll +=weight;}, {"normEvtWeightFinal"});

  auto df_mll_from_susy2 = df_event_weights.Filter("invMassTwoMuons > 110");
  auto mll_from_susy = df_mll_from_susy2.Count();
  float weighted_mll_from_susy = 0;
  df_mll_from_susy2.Foreach([&](double weight){weighted_mll_from_susy +=weight;}, {"normEventWeight"});
  
  //-----------------------------------------------------------------//
  // all cuts + SR H defined by set of regions 1
  auto df_HR1 = df_mll.Filter("invMassTwoMuons > 200 && abs(Muon1TrackD0) > 0.6 && abs(Muon2TrackD0) > 0.6 && abs(Muon1TrackD0) < 3 && abs(Muon2TrackD0) < 3");
  auto HR1 = df_HR1.Count();
  float weighted_HR1 = 0;
  df_HR1.Foreach([&](double weight){weighted_HR1 +=weight;}, {"normEvtWeightFinal"});

  //-----------------------------------------------------------------//
  // all cuts + SR H defined by set of regions 2
  auto df_HR2 = df_mll.Filter("invMassTwoMuons > 140 && abs(Muon1TrackD0) > 0.6 && abs(Muon2TrackD0) > 0.6 && abs(Muon1TrackD0) < 3 && abs(Muon2TrackD0) < 3");
  auto HR2 = df_HR2.Count();
  float weighted_HR2 = 0;
  df_HR2.Foreach([&](double weight){weighted_HR2 +=weight;}, {"normEvtWeightFinal"});
  
  //-----------------------------------------------------------------//
  // all cuts + SR H defined by set of regions 3
  auto df_HR3 = df_mll.Filter("invMassTwoMuons > 125 && abs(Muon1TrackD0) > 0.6 && abs(Muon2TrackD0) > 0.6 && abs(Muon1TrackD0) < 1.3 && abs(Muon2TrackD0) < 1.3 && deltaRTwoMuons > 3");
  auto HR3 = df_HR3.Count();
  float weighted_HR3 = 0;
  df_HR3.Foreach([&](double weight){weighted_HR3 +=weight;}, {"normEvtWeightFinal"});
  
  //-----------------------------------------------------------------//
  // all cuts + in regions A, B, C, E, H

  vector<double> numbers{
    initalSumOfWeights,initalNumEvents,
      selectedSumOfWeights,selectedNumEvents,
      weighted_susy2_2mu, double(*susy2_2mu),
      weighted_trigger, double(*trigger),
      weighted_trig_matched, double(*trig_matched),
      weighted_opp_sign, double(*opp_sign),
      weighted_medium_qual, double(*medium_qual),
      weighted_eta2p5, double(*eta2p5),
      weighted_pt20, double(*pt20),
      weighted_iso, double(*iso),
      weighted_iso_SFs, double(*iso),
      weighted_mll, double(*mll),
      weighted_HR1, double(*HR1),
      weighted_HR2, double(*HR2),
      weighted_HR3, double(*HR3),
      
      weighted_trig_from_susy, double(*trig_from_susy),
      weighted_trig_matched_from_susy, double(*trig_matched_from_susy),
      weighted_opp_sign_from_susy, double(*opp_sign_from_susy),
      weighted_medium_qual_from_susy, double(*medium_qual_from_susy),
      weighted_eta2p5_from_susy, double(*eta2p5_from_susy),
      weighted_pt20_from_susy, double(*pt20_from_susy),
      weighted_iso_from_susy, double(*iso_from_susy),
      weighted_mll_from_susy, double(*mll_from_susy),
      };

  return numbers;
}
void cutflow(std::string mc16a_f, std::string mc16d_f, std::string mc16e_f){
  
  TString mc16a_file = mc16a_f;
  TString mc16d_file = mc16d_f;
  TString mc16e_file = mc16e_f;
  
  vector<double> mc16a_nums = getnums(mc16a_file);
  vector<double> mc16d_nums = getnums(mc16d_file);
  vector<double> mc16e_nums = getnums(mc16e_file);

  vector<double> run2;    
  for(int i=0; i < mc16a_nums.size(); i++){
    double added_val = mc16a_nums[i] + mc16d_nums[i] + mc16e_nums[i];
    run2.push_back(added_val);
  }

  cout <<  "number of events, weighted (raw), each step includes the previous step:" << endl;
  cout << run2[0] << " ( " << run2[1] << " )  number of events produced " << endl;
  cout << run2[2] << " ( " << run2[3] << " )  number of that passed the SUSY2 cuts" << endl;
  cout << run2[4] << " ( " << run2[5] << " )  muons in event + cleaning cuts " << endl;
  cout << run2[6] << " ( " << run2[7] << " ) trigger (2mu14)" << endl;
  cout << run2[8] << " ( " << run2[9] << " ) both muons are trigger matched" << endl;
  cout << run2[10] << " ( " << run2[11] << " ) event is opposite sign" << endl;  
  cout << run2[12] << " ( " << run2[13] << " ) both muons are of at least medium quality" << endl;
  cout << run2[14] << " ( " << run2[15] << " ) both muons have |eta| < 2.5" << endl;
  cout << run2[16] << " ( " << run2[17] << " ) both muons have pT > 20 GeV" << endl;  
  cout << run2[18] << " ( " << run2[19] << " ) both muons are isolated" << endl;  
  cout << run2[20] << " ( " << run2[21] << " ) including scale factors to correct for different in MC and data" << endl;
  cout << run2[22] << " ( " << run2[23] << " ) invariant mass of the two muons > 110 GeV" << endl;
  cout << run2[24] << " ( " << run2[25] << " ) signal region H defined by set of regions 1" << endl;
  cout << run2[26] << " ( " << run2[27] << " ) signal region H defined by set of regions 2" << endl;
  cout << run2[28] << " ( " << run2[29] << " ) signal region H defined by set of regions 3\n" << endl;
  cout <<  "------------------------------------------------------------\n" << endl;
  cout <<  "number of events, weighted (raw), however this time each includes ONLY this SUSY2 cuts, 2 muons in an event, cleaning cuts AND the specified cut:" << endl;
  cout << run2[4] << " ( " << run2[5] << " )  muons in event + cleaning cuts +SUSY2 " << endl;
  cout << run2[30] << " ( " << run2[31] << " ) trigger (2mu14)" << endl;
  cout << run2[32] << " ( " << run2[33] << " ) both muons are trigger matched" << endl;
  cout << run2[34] << " ( " << run2[35] << " ) event is opposite sign" << endl;  
  cout << run2[36] << " ( " << run2[37] << " ) both muons are of at least medium quality" << endl;
  cout << run2[38] << " ( " << run2[39] << " ) both muons have |eta| < 2.5" << endl;
  cout << run2[40] << " ( " << run2[41] << " ) both muons have pT > 20 GeV" << endl;  
  cout << run2[42] << " ( " << run2[43] << " ) both muons are isolated" << endl;  
  cout << run2[44] << " ( " << run2[45] << " ) invariant mass of the two muons > 110 GeV" << endl;

}
