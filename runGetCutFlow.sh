#!/bin/bash

input_file_mc16a=${1}
input_file_mc16d=${2}
input_file_mc16e=${3}
root -b -l -q "cutflow.cxx(\"${input_file_mc16a}\", \"${input_file_mc16d}\", \"${input_file_mc16e}\")";
